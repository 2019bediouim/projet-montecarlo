import numpy as np 
import plotly.graph_objects as go 
from plotly.subplots import make_subplots
from scipy.stats import norm
from scipy.special import factorial


class Rich_Rombrg:
    def __init__(self,R,T) -> None:
        self.R=R
        self.T=T
    def indep_Brown(self,N_sim,N_step):
        U=[0]*self.R 
        for i in range(1,self.R+1):
            np.random.seed()
            U[i-1]=np.random.standard_normal((N_step*i,N_sim))
        return U
    def get_ppcm(self):
        return np.lcm.reduce(np.arange(1,self.R+1))
    def consistant_Brown(self,N_sim,N_step):
        lcm=self.get_ppcm()
        W=np.random.standard_normal(((N_step*lcm),N_sim))
        U=[0]*self.R
        
        for i in range(1,self.R+1):
            
            U[i-1]=np.sqrt(i/lcm)*np.array([z.sum(axis=0) for z in np.split(W, i*N_step)])
            
        return U
    def get_coeff(self,type="1"):

        x=np.arange(1,self.R+1)
        if type =="1":
            return ((-1)**(self.R-x))*((x**self.R)/(factorial(self.R-x)*factorial(x)))
        else: 
            coef=np.array([1+np.sqrt(x/r) for r in x])
            return (((-1)**(self.R-x))/2)*((x**self.R)/(factorial(self.R-x)*factorial(x)))*coef.prod(axis=1)
    

    
class BS_options(Rich_Rombrg): 
    def __init__(self, R,T,K,r,sig) -> None:
        super().__init__(R, T)
        self.sig=sig
        self.K=K
        self.r=r
    
    def d_1(self,x):
        return (np.log(x)+(self.r+self.sig**2/2)*self.T)*(1/self.sig*np.sqrt(self.T))
    def d_2(self,x):
        return self.d_1(x)-self.sig*np.sqrt(self.T)
    def True_Call_BS(self,S):
        return S*norm.cdf(self.d_1(S/self.K))-self.K*np.exp(-self.r*self.T)*norm.cdf(self.d_2(S/self.K))
    def Call_up_out_cont(self,S_0,L):
        return S_0*(norm.cdf(self.d_1(S_0/self.K))-norm.cdf(self.d_1(S_0/L)))\
            -self.K*np.exp(-self.r*self.T)*(norm.cdf(self.d_2(S_0/self.K))-norm.cdf(self.d_2(S_0/L)))\
                -L*(S_0/L)**(-2*self.r/self.sig**2)*(norm.cdf(self.d_1(L**2/(self.K*S_0)))-norm.cdf(self.d_1(L/S_0)))\
                    +self.K*np.exp(-self.r*self.T)*(S_0/L)**(1-2*self.r/self.sig**2)*(norm.cdf(self.d_2(L**2/(self.K*S_0)))-norm.cdf(self.d_2(L/S_0)))
   

    def Euler_scheme(self,S,N_step,U,compute_max=False):
        S_T=S
        if compute_max==True:
            M=S
            for i in range(N_step):
                S_T*=(1+self.r*(self.T/N_step))+self.sig*np.sqrt(self.T/N_step)*U[i]
                M=np.where(S_T>M,S_T,M)
            return S_T,M
        for i in range(N_step):
            S_T*=(1+self.r*(self.T/N_step))+self.sig*np.sqrt(self.T/N_step)*U[i]
        return S_T
    
    def Barrier_BS(self,S,L,N_sim,N_step,Scheme="c",opt_type="up_in"):

        
        if Scheme=="c":
            b=np.log(L/S)/self.sig
            U=self.consistant_Brown(N_sim,N_step)
            alpha=self.get_coeff()
            estimator=0
            for i in range(1,self.R+1):
                S_T=self.Euler_scheme(S,i*N_step,U[i-1])
                X=np.log(S_T/S)/self.sig
                Unif=np.random.uniform(size=int(N_sim))
                M=(X+np.sqrt(X**2-2*self.T*np.log(Unif)))/2
                if opt_type=="up_in":
                    estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)*np.where(M-b<0,0,1)
                elif opt_type=="up_out": 
                    estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)*np.where(M-b>=0,0,1)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)
        elif Scheme=="d":
            U=self.consistant_Brown(N_sim,N_step)
            alpha=self.get_coeff("2")
            estimator=0
            for i in range(1,self.R+1):
                S_T,M=self.Euler_scheme(S,i*N_step,U[i-1],compute_max=True)
                if opt_type=="up_in":
                    estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)*np.where(M-L<0,0,1)
                elif opt_type=="up_out": 
                    estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)*np.where(M-L>=0,0,1)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)
        else :
            b=np.log(L/S)/self.sig
            equiv_cplx=N_step*self.R*(self.R+1)//2
            U=np.random.standard_normal((equiv_cplx,N_sim))
            S_T=self.Euler_scheme(S,equiv_cplx,U)
            X=np.log(S_T/S)/self.sig
            Unif=np.random.uniform(size=int(N_sim))
            M=(X+np.sqrt(X**2-2*self.T*np.log(Unif)))/2
            if opt_type=="up_in":
                estimator=np.fmax(S_T-self.K,0)*np.where(M-b<0,0,1)
            elif opt_type=="up_out": 
                estimator=np.fmax(S_T-self.K,0)*np.where(M-b>=0,0,1)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)
  



    def RR_Call_BS(self,S,N_sim,N_step,Increments="consist"):

        if Increments=="consist":
            U=self.consistant_Brown(N_sim,N_step)
            alpha=self.get_coeff()
        
            estimator=0
            for i in range(1,self.R+1):
                S_T=self.Euler_scheme(S,i*N_step,U[i-1])
                estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)
        elif Increments=="indep": 
            U=self.indep_Brown(N_sim,N_step)
            alpha=self.get_coeff()
            estimator=0
            for i in range(1,self.R+1):
                S_T=self.Euler_scheme(S,i*N_step,U[i-1])
                estimator+=alpha[i-1]*np.fmax(S_T-self.K,0)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)
        else :
            equiv_cplx=N_step*self.R*(self.R+1)//2
            U=np.random.standard_normal((equiv_cplx,N_sim))
            S_T=self.Euler_scheme(S,equiv_cplx,U)
            estimator=np.fmax(S_T-self.K,0)
            price=estimator.mean()
            std2=np.sum((estimator-price)**2)/(N_sim-1)
            return np.exp(-self.r*self.T)*price, np.sqrt(std2)

    def compare(self,S,L,N_sim,N_range,show_Euler=False,instrument="Call"): 
        std_consist=[]
        prima_consist=[]
        std_indep=[]
        prima_indep=[]

        std_euler=[]
        prima_euler=[]

        if instrument=="Call":
            for n in (N_range): 
                consist=self.RR_Call_BS(S,N_sim,n)
                indep=self.RR_Call_BS(S,N_sim,n,"indep")
                if show_Euler==True: 
                    euler=self.RR_Call_BS(S,N_sim,n,"euler")
                    std_euler.append(euler[1])
                    prima_euler.append(euler[0])
                std_consist.append(consist[1])
                prima_consist.append(consist[0])
                std_indep.append(indep[1])
                prima_indep.append(indep[0])
                

            fig=make_subplots(rows=1, cols=2,subplot_titles=["Premium-BS_Value","Standard Deviation"])
            abcisses=self.R*N_range
            if show_Euler==True: 
                abcisses=N_range*self.R*(self.R+1)/2
                fig.add_trace(go.Scatter(x=abcisses, y=std_euler, mode='markers',marker_symbol=3,name="Std with stanadrd Euler scheme"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_euler-self.True_Call_BS(S), mode='lines+markers',marker_symbol=3,name="Premium with stanadrd Euler scheme"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_consist-self.True_Call_BS(S), mode='lines+markers',marker_symbol=5,name="Premium with consistant increments"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=std_consist, mode='markers',marker_symbol=5,name="Std with consistant increments"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=[0]*len(N_range), mode='lines',marker_symbol=4,name="Premium True Value",line=dict(width=2,
                                    dash='dash')),row=1,col=1)
            else: 
                fig.add_trace(go.Scatter(x=abcisses, y=prima_consist-self.True_Call_BS(S), mode='lines+markers',marker_symbol=5,name="Premium with consistant increments"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_indep-self.True_Call_BS(S), mode='lines+markers',marker_symbol=4,name="Premium with independant increments"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=std_consist, mode='markers',marker_symbol=5,name="Std with consistant increments"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=std_indep, mode='markers',marker_symbol=4,name="Std with independant increments"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=[0]*len(N_range), mode='lines',marker_symbol=4,name="Premium True Value",line=dict(width=2,
                                    dash='dash')),row=1,col=1)
        if instrument=="Barrier":
            for n in (N_range): 
                consist=self.Barrier_BS(S,L,N_sim,n,"c","up_out")
                indep=self.Barrier_BS(S,L,N_sim,n,"d","up_out")
                if show_Euler==True: 
                     euler=self.Barrier_BS(S,L,N_sim,n,"euler","up_out")
                     std_euler.append(euler[1])
                     prima_euler.append(euler[0])
                std_consist.append(consist[1])
                prima_consist.append(consist[0])
                std_indep.append(indep[1])
                prima_indep.append(indep[0])
                

            fig=make_subplots(rows=1, cols=2,subplot_titles=["Premium-BS_Value","Standard Deviation"])
            abcisses=self.R*N_range
            if show_Euler==True: 
                abcisses=N_range*self.R*(self.R+1)/2
                fig.add_trace(go.Scatter(x=abcisses, y=std_euler, mode='markers',marker_symbol=3,name="Std with  continuous Euler scheme"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_euler-self.Call_up_out_cont(S,L), mode='lines+markers',marker_symbol=3,name="Premium with  continuous Euler scheme"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_consist-self.Call_up_out_cont(S,L), mode='lines+markers',marker_symbol=5,name="Premium R-R extrapolation  of the continuous Euler scheme"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=std_consist, mode='markers',marker_symbol=5,name="Std R-R extrapolation  of the continuous Euler scheme"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=[0]*len(N_range), mode='lines',marker_symbol=4,name="Premium True Value",line=dict(width=2,
                                    dash='dash')),row=1,col=1)
            else: 
                
                fig.add_trace(go.Scatter(x=abcisses, y=prima_consist-self.Call_up_out_cont(S,L), mode='lines+markers',marker_symbol=5,name="Premium R-R extrapolation  of the continuous Euler scheme"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=prima_indep-self.Call_up_out_cont(S,L), mode='lines+markers',marker_symbol=4,name="Premium R-R extrapolation of the stepwise constant Euler scheme"),row=1,col=1)
                fig.add_trace(go.Scatter(x=abcisses, y=std_consist, mode='markers',marker_symbol=5,name="Std R-R extrapolation  if the continuous Euler scheme"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=std_indep, mode='markers',marker_symbol=4,name="Std R-R extrapolation of the stepwise constant Euler scheme"),row=1,col=2)
                fig.add_trace(go.Scatter(x=abcisses, y=[0]*len(N_range), mode='lines',marker_symbol=4,name="Premium True Value",line=dict(width=2,
                                    dash='dash')),row=1,col=1)
        
        fig.show()
def compare_R_order(S,N_sim,N_range,R,T,K,r,sig): 
        vals_prima=[]
        vals_std=[]

        for R_i in R:
            prima_R=[]
            std_R=[]
            call=BS_options(R_i,T,K,r,sig)
            for n in (N_range): 
                val=call.RR_Call_BS(S,N_sim,n)
                prima_R.append(val[0])
                std_R.append(val[1])
            vals_prima.append(prima_R)
            vals_std.append(std_R)

        fig=make_subplots(rows=1, cols=2,subplot_titles=["Premium-BS_Value","Standard Deviation"])
        max_order=max(R)
        equiv_cplx=max_order*(max_order+1)//2
        abcisses=equiv_cplx*N_range
        C_0=call.True_Call_BS(S)
        for i in range(len(R)):

            fig.add_trace(go.Scatter(x=abcisses, y=vals_std[i], mode='markers',marker_symbol=3,name=f"Std with R={R[i]}"),row=1,col=2)
            fig.add_trace(go.Scatter(x=abcisses, y=vals_prima[i]-C_0, mode='lines+markers',marker_symbol=3,name=f"Premium with R={R[i]}"),row=1,col=1)
        
        fig.add_trace(go.Scatter(x=abcisses, y=[0]*len(N_range), mode='lines',marker_symbol=4,name="Premium True Value",line=dict(width=2,
                                dash='dash')),row=1,col=1)
        fig.show()